#include <sstream>
#include <WaterDrum.h>
#include <DrumGame.h>

WaterDrum* createDrums(int numDrums)
{
	WaterDrum* waterDrums = new WaterDrum[numDrums];

	for (int i = 0; i < numDrums; i++) {
		float y = resolution.y/7*2;
			if (i%2){
				y = resolution.y/7*4;
			}
		// Spawn the new water drum into the array			
		waterDrums[i].spawn(resolution.x/30*i, y, i);

	}
	return waterDrums;
}
