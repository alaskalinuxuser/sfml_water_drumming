# Water Drumming!

More of a theory to concept, this simple game is testing my SFML skills to create a fun little water drum game. All of the artwork was made by me, all of this project falls under the Apache 2.0 license, even the font. Enjoy!

# Installation

Download this repository, extract, and then run the build.sh script. Requires libsfml!

# Note about sound

I recommend having a good set of headphones or a subwoofer for this game. The drum sounds are not very distinct on smaller headsets or cheap speakers, but they really come to life with the bass cranked up!

# How to play

- Right click or Escape key will exit the game at any time.
- Left click to activate a button or hit a drum.
- FreePlay is simply a mode where you can drum up a storm on your own. Make some cool sounds!
- Memory is a game where the computer will generate a pattern that you must repeat on the drums.

