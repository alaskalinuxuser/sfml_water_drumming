#include <sstream>
#include <MenuItems.h>
#include <TextureHolder.h>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;


void MenuItems::spawn(float startX, float startY, int itemNum) {
	
	// Find the screen resolution
	Vector2f sResolution;
	sResolution.x = VideoMode::getDesktopMode().width;
	sResolution.y = VideoMode::getDesktopMode().height;
	
	m_Sprite = Sprite(TextureHolder::GetTexture(
		"graphics/menu_item.png"));

	m_Position.x = startX - 125;
	m_Position.y = startY;
	m_Sprite.setPosition(m_Position);
	m_Sprite.setScale((sResolution.x*.2)/500,(sResolution.x*.2)/500);
	m_Active = false;
	m_Highlight = false;
	m_Pressed = false;
	m_Motion = 0;
	m_Number = itemNum;
}

void MenuItems::struck() {
	// Press the button.
	m_Active = true;
}

void MenuItems::highlighted(bool highlightMe) {
	// Highlight the button.
	m_Highlight = highlightMe;
}

bool MenuItems::isActive() {
	return m_Active;
}

bool MenuItems::isPressed() {
	return m_Pressed;
}

FloatRect MenuItems::getPosition() {
	return m_Sprite.getGlobalBounds();
}

Vector2f MenuItems::getCoordPosition() {
	return m_Sprite.getPosition();
}

Sprite MenuItems::getSprite() {
	return m_Sprite;
}

void MenuItems::update(float elapsedTime, float sResolution) {

	if (m_Highlight) {
		
		// Let's make it bigger since the button is highlighted!
		m_Sprite.setScale((sResolution*.2)/500,(sResolution*.25)/500);
		m_Sprite.setPosition(m_Position);
	
	} else {
		
		// Since it is not highlighted, make it normal.
		// Apperantly we have to update the scale when we change the sprite.
		m_Sprite.setScale((sResolution*.2)/500,(sResolution*.2)/500);
		m_Sprite.setPosition(m_Position);
	}
	
	if (m_Active) {
		
		// Since the button was pressed, let's do some motion!
		m_Motion = m_Motion + elapsedTime;
		float motionCount = m_Motion;
		
		if (motionCount == 1) {
			m_Position.x = m_Position.x + 2.5;
			m_Position.y = m_Position.y + 2.5;
			m_Sprite.setPosition(m_Position);
		} else if (motionCount <= .15) {
			m_Position.x = m_Position.x + 2.5;
			m_Position.y = m_Position.y + 2.5;
			m_Sprite.setPosition(m_Position);
		} else if (motionCount <= .30) {
			m_Position.x = m_Position.x - 5.0;
			m_Position.y = m_Position.y - 5.0;
			m_Sprite.setPosition(m_Position);
		} else if (motionCount <= .45) {
			m_Position.x = m_Position.x + 5.0;
			m_Position.y = m_Position.y;
			m_Sprite.setPosition(m_Position);
		} else if (motionCount <= .60) {
			m_Position.x = m_Position.x - 5.0;
			m_Position.y = m_Position.y + 5.0;
			m_Sprite.setPosition(m_Position);
		} else if (motionCount <= .75) {
			m_Position.x = m_Position.x + 2.5;
			m_Position.y = m_Position.y + 2.5;
			m_Sprite.setPosition(m_Position);
		} else if (motionCount <= .90) {
			m_Position.x = m_Position.x;
			m_Position.y = m_Position.y -5.0;
			m_Sprite.setPosition(m_Position);
		} else {
			m_Position.x = m_Position.x;
			m_Position.y = m_Position.y;
			m_Sprite.setPosition(m_Position);
		}
	} else {
		
		// What else do we need to do with this?
	}
	
	if (m_Motion >= 1.25) {
			m_Motion = 0;
			m_Active = false;
			m_Pressed = true;
		}

}
