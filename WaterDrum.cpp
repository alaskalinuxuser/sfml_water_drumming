#include <sstream>
#include <WaterDrum.h>
#include <TextureHolder.h>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;


void WaterDrum::spawn(float startX, float startY, int drumNum) {
	
	// Find the screen resolution
	Vector2f sResolution;
	sResolution.x = VideoMode::getDesktopMode().width;
	sResolution.y = VideoMode::getDesktopMode().height;
	
	m_Sprite = Sprite(TextureHolder::GetTexture(
		"graphics/waterdrum_still.png"));

	m_Position.x = startX;
	m_Position.y = startY;
	m_Sprite.setPosition(m_Position);
	m_Sprite.setScale((sResolution.x*.2)/500,(sResolution.x*.2)/500);
	m_Motion = 0;
	m_Number = drumNum;
}

void WaterDrum::struck() {
	// Set the motion counter back to 30.
	m_Motion = 0;

	// And still bobbing around after being struck.
	m_Active = true;
}

bool WaterDrum::isActive() {
	return m_Active;
}

FloatRect WaterDrum::getPosition() {
	return m_Sprite.getGlobalBounds();
}

Vector2f WaterDrum::getCoordPosition() {
	return m_Sprite.getPosition();
}

Sprite WaterDrum::getSprite() {
	return m_Sprite;
}

void WaterDrum::update(bool stillActive, float elapsedTime, float sResolution) {

	if (stillActive) {
		
		m_Motion = m_Motion + elapsedTime;
		float motionCount = m_Motion;
		
		// Update the sprite if struck.
		if (motionCount == 1.75) {
			m_Sprite = Sprite(TextureHolder::GetTexture(
			"graphics/waterdrum_still.png"));
		} else if (motionCount <= .25) {
			m_Sprite = Sprite(TextureHolder::GetTexture(
			"graphics/waterdrum_1.png"));
		} else if (motionCount <= .5) {
			m_Sprite = Sprite(TextureHolder::GetTexture(
			"graphics/waterdrum_2.png"));
		} else if (motionCount <= .75) {
			m_Sprite = Sprite(TextureHolder::GetTexture(
			"graphics/waterdrum_3.png"));
		} else if (motionCount <= 1) {
			m_Sprite = Sprite(TextureHolder::GetTexture(
			"graphics/waterdrum_4.png"));
		} else if (motionCount <= 1.25) {
			m_Sprite = Sprite(TextureHolder::GetTexture(
			"graphics/waterdrum_5.png"));
		} else if (motionCount <= 1.5) {
			m_Sprite = Sprite(TextureHolder::GetTexture(
			"graphics/waterdrum_6.png"));
		} else {
			m_Sprite = Sprite(TextureHolder::GetTexture(
				"graphics/waterdrum_still.png"));
		}
	} else {
		m_Sprite.setTexture(TextureHolder::GetTexture(
			"graphics/waterdrum_still.png"));
		
	}
	
	if (m_Motion >= 1.75)
		{
			m_Motion = 0;
			m_Active = false;
		}	
	
	// Apperantly we have to update the scale when we change the sprite.
	m_Sprite.setScale((sResolution*.2)/500,(sResolution*.2)/500);
	m_Sprite.setPosition(m_Position);
	


}
