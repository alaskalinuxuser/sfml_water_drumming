#pragma once
#include <SFML/Graphics.hpp>
#include <cmath>

using namespace sf;

class MenuItems {
	
private:
	
	// Where is this MenuItems?
	Vector2f m_Position;

	// A sprite for the MenuItems
	Sprite m_Sprite;

	// Motion counter.
	float m_Motion;

	// Is it still active?
	bool m_Active;
	
	// Is it Highlighted?
	bool m_Highlight;
	
	// Is it pressed?
	bool m_Pressed;
	
	int m_Number;

	// Public prototypes go here	
public:

	// Spawn a new MenuItems
	void spawn(float startX, float startY, int itemNum);

	// Handle when a MenuItems is struck
	void struck();
	
	// Handle when a MenuItems is highlighted
	void highlighted(bool highlightMe);	

	// Find out if the MenuItems is active
	bool isActive();
	
	// We will return a bool to say if it has finished being pressed.
	// This enables our cool graphic to run, then do what it asked for.
	bool isPressed();

	// Return a rectangle that is the position in the world
	FloatRect getPosition();
	
	// Return coordinate position
	Vector2f getCoordPosition();

	// Get a copy of the sprite to draw
	Sprite getSprite();

	// Update the MenuItems each frame
	void update(float elapsedTime, float sResolution);
};


