#pragma once
#include <SFML/Graphics.hpp>
#include <cmath>

using namespace sf;

class WaterDrum {
	
private:
	
	// Where is this WaterDrum?
	Vector2f m_Position;

	// A sprite for the WaterDrum
	Sprite m_Sprite;

	// Motion counter.
	float m_Motion;

	// Is it still alive?
	bool m_Active = false;
	
	int m_Number;

	// Public prototypes go here	
public:

	// Handle when a water drum is struck
	void struck();

	// Find out if the water drum is active
	bool isActive();

	// Spawn a new water drum
	void spawn(float startX, float startY, int drumNum);

	// Return a rectangle that is the position in the world
	FloatRect getPosition();
	
	// Return coordinate position
	Vector2f getCoordPosition();

	// Get a copy of the sprite to draw
	Sprite getSprite();

	// Update the WaterDrum each frame
	void update(bool stillActive, float elapsedTime, float sResolution);
};


