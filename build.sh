#!/bin/bash

# You need libsfml installed on your system!

g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 TextureHolder.cpp -o TextureHolder.o
g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 WaterDrum.cpp -o WaterDrum.o
g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 MenuItems.cpp -o MenuItems.o
g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 ExitButton.cpp -o ExitButton.o
g++ -I./  -Wall -ansi -Wextra -g -c -std=c++14 DrumGame.cpp -o DrumGame.o


g++ TextureHolder.o WaterDrum.o MenuItems.o ExitButton.o DrumGame.o -o DrumGame-app -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

./DrumGame-app

echo "type ./DrumGame-app to launch"
exit
