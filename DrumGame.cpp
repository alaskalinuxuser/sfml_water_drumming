#include <SFML/Graphics.hpp>
#include <sstream>
#include <SFML/Audio.hpp>
#include <cmath>
#include <TextureHolder.h>
#include <WaterDrum.h>
#include <MenuItems.h>
#include <ExitButton.h>
#include <string>
#include <iostream>

// Declare the functions....
using namespace sf;

// The main process....
int main()
{
	TextureHolder holder;
	
	// What is the game state?
	enum class State {
		FREESTYLE, MEMORY, DEMO, INTRO, MENU
	};
	
	// We will start with the game over state.
	State state = State::INTRO;
	
	// Find the screen resolution
	Vector2f resolution;
	resolution.x = VideoMode::getDesktopMode().width;
	resolution.y = VideoMode::getDesktopMode().height;

	
	// And use it to create an SFML window
	RenderWindow window(VideoMode(resolution.x, resolution.y), 
		"Water Drumming!", Style::Fullscreen);
		
	// Create a an SFML View for the main action
	View mainView(FloatRect(0, 0, resolution.x, resolution.y));
	
	// Create a an SFML View for the HUD
	View hudView(FloatRect(0, 0, resolution.x, resolution.y));

	// Time control
	Clock clock;

	// Total runtime of Playing state.
	Time gameTimeTotal;
	
	// Where is the mouse in relation to the virtual world
	Vector2f mouseWorldPosition;
	Vector2f mouseHudPosition;
	// Where is the mouse in relation to the real screen
	Vector2i mouseScreenPosition;
	
	// Hide the mouse pointer and replace it with drum hand
	window.setMouseCursorVisible(false);
	Texture textureDrumHand = TextureHolder::GetTexture("graphics/hand.png");
	Sprite spriteDrumHand;
	spriteDrumHand.setTexture(textureDrumHand);
	spriteDrumHand.setOrigin(100, 100);
	spriteDrumHand.setScale((resolution.x*.04)/200,(resolution.x*.04)/200);

	// The demo hand.
	Texture textureRHand = TextureHolder::GetTexture("graphics/rhand.png");
	Sprite spriteRHand;
	spriteRHand.setTexture(textureRHand);
	spriteRHand.setOrigin(1, 1);
	spriteRHand.setScale((resolution.x*.04)/50,(resolution.x*.04)/50);
	spriteRHand.setPosition(-1000,-1000);
    
    // Create a texture for our background and load it.
	Texture textureBackground = TextureHolder::GetTexture("graphics/background.png");
    Sprite spriteBackground;
    spriteBackground.setTexture(textureBackground);
    spriteBackground.setScale(resolution.x/640, resolution.y/480);
    spriteBackground.setPosition(0,0);
    
    // And the water drums themselves.
    int numDrums = 5;
    WaterDrum* waterDrums = nullptr;
    
    // And the menu items
	int numMenuItems = 4;
	MenuItems* menuItems = nullptr;
	
	// And the menu items
	int numExitButtons = 1;
	ExitButton* exitButton = nullptr;

    
    // Add a font.
    Font font;
    font.loadFromFile("fonts/FogSans.ttf");
    
    // Add failText
    Text failText;
    failText.setString("Oops! You lose! \nClick to return to the main menu!");
    failText.setCharacterSize(resolution.y/20);
    failText.setColor(Color::White);
    failText.setFont(font);
    // Set position based on center
    FloatRect failTextRect = failText.getLocalBounds();
    failText.setOrigin(failTextRect.left + failTextRect.width / 2.0f,
    failTextRect.top + failTextRect.height / 2.0f);
    failText.setPosition(-2000,-2000);
    
    // Add greatJobText
    Text greatJobText;
    greatJobText.setString("Great Job! \nClick to continue!");
    greatJobText.setCharacterSize(resolution.y/20);
    greatJobText.setColor(Color::White);
    greatJobText.setFont(font);
    // Set position based on center
    FloatRect gjTextRect = greatJobText.getLocalBounds();
    greatJobText.setOrigin(gjTextRect.left + gjTextRect.width / 2.0f,
    gjTextRect.top + gjTextRect.height / 2.0f);
    greatJobText.setPosition(-2000,-2000);
    
    // Add readyText
    Text readyText;
    readyText.setString("Click when ready!");
    readyText.setCharacterSize(resolution.y/20);
    readyText.setColor(Color::White);
    readyText.setFont(font);
    // Set position based on center
    FloatRect readyTextRect = readyText.getLocalBounds();
    readyText.setOrigin(readyTextRect.left + readyTextRect.width / 2.0f,
    readyTextRect.top + readyTextRect.height / 2.0f);
    readyText.setPosition(-2000,-2000);
    
    // Add menuText
    Text menuText;
    menuText.setString("Main Menu");
    menuText.setCharacterSize(resolution.y/20);
    menuText.setColor(Color::White);
    menuText.setFont(font);
    menuText.setPosition(resolution.x /2.0f,resolution.y / 6 * 2);
    
    // Add freeStyleText
    Text freeStyleText;
    freeStyleText.setString("Free-Style");
    freeStyleText.setCharacterSize(resolution.y/20);
    freeStyleText.setColor(Color::White);
    freeStyleText.setFont(font);
    freeStyleText.setPosition(resolution.x /2.0f,resolution.y / 6 * 3);
    
    // Add memoryText
    Text memoryText;
    memoryText.setString("Memory");
    memoryText.setCharacterSize(resolution.y/20);
    memoryText.setColor(Color::White);
    memoryText.setFont(font);
    memoryText.setPosition(resolution.x /2.0f,resolution.y / 6 * 4);
    
    // Add exitText
    Text exitText;
    exitText.setString("Exit");
    exitText.setCharacterSize(resolution.y/20);
    exitText.setColor(Color::White);
    exitText.setFont(font);
    exitText.setPosition(resolution.x /2.0f,resolution.y / 6 * 5);
    
    // Add scoreText
    int scoreInt = 0;
    Text scoreText;
    scoreText.setString("Score = 0");
    scoreText.setCharacterSize(resolution.y/40);
    scoreText.setColor(Color::White);
    scoreText.setFont(font);
    scoreText.setPosition(10,40);    

    // Add tryText
    int tryInt = 3;
    Text tryText;
    tryText.setString("Tries left = 3");
    tryText.setCharacterSize(resolution.y/40);
    tryText.setColor(Color::White);
    tryText.setFont(font);
    tryText.setPosition(10,resolution.y - 100); 
    
    // Accept keyboard input.
    bool acceptInput = true;   
    bool hasDrummed = false; 
    bool greatJob = false;
    // We will use accumulative time in Demo mode.
    float timeInGame = 0;
    float timeTarget = .5;
    // And a round number in Demo Mode;
    int roundNum = 1;
    int currentDemo = 0;
    // And we need a string to hold our demo mode order.
    std::string demoMode = "";
    std::string memMode = "";
    // And a boolean if the demo has already been shown or not.
    bool demoShown = false;
    bool demoReady = false;
    bool demoSound = true;
    
    // Add a time bar
    RectangleShape timeBar;
    float timeBarStartWidth = resolution.x/4.8;
    float timeBarHeight = resolution.x/48;
    timeBar.setSize(Vector2f(timeBarStartWidth,timeBarHeight));
    timeBar.setFillColor(sf::Color::Green);
    timeBar.setPosition(resolution.x / 2.0f - (timeBarStartWidth / 2.0f), 25);
    float widthPerSecond = timeBarStartWidth / timeTarget;
    
	// Prepare the sounds
	SoundBuffer waterDrumOne;
	waterDrumOne.loadFromFile("sound/waterdrumone.wav");
	Sound drumOne;
	drumOne.setBuffer(waterDrumOne);
	
	SoundBuffer waterDrumTwo;
	waterDrumTwo.loadFromFile("sound/waterdrumtwo.wav");
	Sound drumTwo;
	drumTwo.setBuffer(waterDrumTwo);
	
	SoundBuffer waterDrumThree;
	waterDrumThree.loadFromFile("sound/waterdrumthree.wav");
	Sound drumThree;
	drumThree.setBuffer(waterDrumThree);
	
	SoundBuffer waterDrumFour;
	waterDrumFour.loadFromFile("sound/waterdrumfour.wav");
	Sound drumFour;
	drumFour.setBuffer(waterDrumFour);
	
	SoundBuffer waterDrumFive;
	waterDrumFive.loadFromFile("sound/waterdrumfive.wav");
	Sound drumFive;
	drumFive.setBuffer(waterDrumFive);
	

    while (window.isOpen()) {		
		/* 
		 * 
		 *  Detect input.
		 * 
		 */
			if (Keyboard::isKeyPressed(Keyboard::Escape)) {
				// Escape key is pressed: Exit the window.
				window.close();
			}
			
			// This is now true in all states.
			if (acceptInput) {
					
					if (Mouse::isButtonPressed(Mouse::Left)) {
						
						acceptInput = false;
						hasDrummed = true;
					}	
			} // end acceptInput

			if (Mouse::isButtonPressed(Mouse::Right)) {
				// right mouse button is pressed: exit
				window.close();
			}
			
			Event event;
			while (window.pollEvent(event)) {
				
				if (event.type == sf::Event::MouseButtonReleased) {
					// Listen for key presses again
					acceptInput = true;
					hasDrummed = false;
				}
			}
        
        /* 
		 * 
		 *  Updating the scene.
		 * 
		 */
		
		// These first things are stateless dependant. They must always be.
		Time dt = clock.restart();
		
		// Make a decimal fraction of 1 from the delta time
		float dtAsSeconds = dt.asSeconds();
		
		// Where is the mouse pointer
		mouseScreenPosition = Mouse::getPosition();

		// Convert mouse position to world coordinates of mainView
		mouseWorldPosition = window.mapPixelToCoords(
			Mouse::getPosition(), mainView);
			
		// Convert mouse position to world coordinates of hudView
		mouseHudPosition = window.mapPixelToCoords(
			Mouse::getPosition(), hudView);
						
		// Set the crosshair to the mouse world location
		spriteDrumHand.setPosition(mouseWorldPosition);
			
        if (state == State::INTRO) {
			// Maybe we could program in a cool little intro thing.
			// For now, we'll use this for setup.
			
			// Set our variables.
			scoreInt = 0;
			tryInt = 3;
			acceptInput = true;   
			hasDrummed = false; 
			timeInGame = 0;
			timeTarget = 0;
			roundNum = 1;
			currentDemo = 0;
			demoMode = "";
			memMode = "";
			demoShown = false;
			demoReady = false;
			demoSound = true;
			greatJob = false;
			
			delete[] menuItems;
			menuItems = new MenuItems[numMenuItems];
				for (int i = 0; i < numMenuItems; i++) {
					
					// Spawn the new menu item buttons into the array			
					menuItems[i].spawn(resolution.x/2, resolution.y / 6 * (i + 1), i);
				}
				
			// And build the exitButton
			delete[] exitButton;
			exitButton = new ExitButton[numExitButtons];
			exitButton[0].spawn(resolution.x, resolution.y, numExitButtons);
			
			// And move to the Menu state, since we are done here.
			state = State::MENU;
			
		} else if (state == State::MENU) {
						
			// Let's update the menu items.
			for (int i = 0; i < numMenuItems; i++) {
				
				// Make sure this variable is false to start.
				bool isHighlighted = false;
				
					// If the mouse is over the button, then mark the bool.
					if (menuItems[i].getPosition().contains
						(mouseWorldPosition)) {
							isHighlighted = true;
					}
				menuItems[i].highlighted(isHighlighted);
					
				if (isHighlighted && hasDrummed) {
					menuItems[i].struck();
					hasDrummed = false;
				}
				
				// And update our menuButtons....
				menuItems[i].update(dtAsSeconds, resolution.x);
				
				// And put the words over the buttons.
				menuText.setPosition(menuItems[0].getCoordPosition());
				freeStyleText.setPosition(menuItems[1].getCoordPosition());
				memoryText.setPosition(menuItems[2].getCoordPosition());
				exitText.setPosition(menuItems[3].getCoordPosition());
				
				if (menuItems[i].isPressed()) {
					// Menu button was pressed.
						switch (i) {
							case 3:
								window.close();
								break;

							case 2:
								state = State::DEMO;
								
								// Here we create our water drums for the game.
								delete[] waterDrums;
								waterDrums = new WaterDrum[numDrums];
								for (int i = 0; i < numDrums; i++) {
									float y = resolution.y/7*2;
										if (i%2){
											y = resolution.y/7*4;
										}
									// Spawn the new water drum into the array			
									waterDrums[i].spawn(resolution.x/7*(i+1), y, i);

									}
									
								break;
								
							case 1:
								state = State::FREESTYLE;
								if (state == State::FREESTYLE) {
								
								// Here we create our water drums for the game.
								delete[] waterDrums;
								waterDrums = new WaterDrum[numDrums];
								for (int i = 0; i < numDrums; i++) {
									float y = resolution.y/7*2;
										if (i%2){
											y = resolution.y/7*4;
										}
									// Spawn the new water drum into the array			
									waterDrums[i].spawn(resolution.x/7*(i+1), y, i);

									}
								}
								break;
								
							default:
								state = State::INTRO;
								break;
							}
				} // End if pressed button.
			} // End for loop of menu items.
		
		} else if (state == State::DEMO) {
			
			// So we need a new number added to the list.
			// Add the time between frames into the time in game.
			timeInGame += dtAsSeconds;
			
			if (hasDrummed) {
				if (exitButton[0].getPosition().contains
						(mouseWorldPosition)) {
					// Since they clicked to exit, take them to the menu.
					state = State::INTRO;
				}
			}
					
			if (!demoShown) {
				if (!demoReady) {
					// Reset the game time counter, since it is not ready....
					timeInGame = 0;
					readyText.setPosition(-2000,-2000);

					
					// Generate a random number that is one of the drums.
					srand((int)time(0) + roundNum);
					int r = (rand() % 5);
					std::string s = std::to_string(r);
					demoMode = demoMode + s;
					
					// For troubleshooting //
					 // std::cout << " * ";
					 // std::cout << r;
					 // std::cout << " * ";
					 // std::cout << s;
					 // std::cout << " * ";
					 // std::cout << demoMode;
					 // std::cout << " * ";
					
					if (roundNum == demoMode.length()) {
						// We are good, so set demo ready to true, so we stop
						// adding characters.
						demoReady = true;
					} else if (roundNum < demoMode.length()) {
						/* 
						 * This helps when repeating the demo of a round.
						 */
						std::string tempString = "";
						for (int i = 0; i < roundNum; i++) {
							tempString = tempString + demoMode.at(i);
						}
						// And rewrite demoMode with tempString.
						demoMode = tempString;
						demoReady = true;
						
					} else {
						// The roundNum is greater than the string length,
						// so we need more numbers added to our string!
						demoReady = false;
					}
					
					// We want two displays of the demo per second.
					timeTarget = 1 * roundNum;
					widthPerSecond = timeBarStartWidth / timeTarget;
					// So if we are on round 6, we have a 3 second time target.
					// And set our current demo counter to 1.
					currentDemo = 1;
						// End Demo is ready.
				} else {
				
					switch (demoMode.at(currentDemo - 1)) {
					case '0':
						// Set the drumhand to the location of the waterdrum. So they "see" it.
						spriteRHand.setPosition(waterDrums[0].getCoordPosition());
						// Play a sound.
						if (demoSound) {
							drumOne.play();
							// Turn off the sound so it doesn't repeat.
							demoSound = false;
						}
						break;

					case '1':
						spriteRHand.setPosition(waterDrums[1].getCoordPosition());
						if (demoSound) {
							drumTwo.play();
							demoSound = false;
						}
						break;
						
					case '2':
						spriteRHand.setPosition(waterDrums[2].getCoordPosition());
						if (demoSound) {
							drumThree.play();
							demoSound = false;
						}
						break;
						
					case '3':
						spriteRHand.setPosition(waterDrums[3].getCoordPosition());
						if (demoSound) {
							drumFour.play();
							demoSound = false;
						}
						break;

					default:
						spriteRHand.setPosition(waterDrums[4].getCoordPosition());
						if (demoSound) {
							drumFive.play();
							demoSound = false;
						}
						break;
					}
				
				// If it has been half a second, increase to the next demo character/drum.
				if (timeInGame >= (currentDemo * 1)) {
					if (currentDemo == demoMode.length()) {
						// Set the text message!
						readyText.setPosition(resolution.x /2.0f,resolution.y / 2);

					} else {
						currentDemo++;
						demoSound = true;
					}
				}
				
				if (timeInGame >= ((currentDemo * 1) +.25)) {
					// In between, move the hand away.
					spriteRHand.setPosition(-1000,-1000);
				}
				
				// If we've shown them all, then we are done. But we give a little
				// "extra" time for the last one to be displayed.
				if (timeInGame >= (timeTarget + .5)) {
					demoShown = true;
				}
					// End demo is shown.
			}} else {
				
				// What should we do before the player has to repeat the drums?
				// How about hide the drumRHand?
				spriteRHand.setPosition(-1000,-1000);
				
				if (hasDrummed) {
				// And set the state to memory!
				state = State::MEMORY;
				acceptInput = false;
				hasDrummed = false;
				// And reset our tracker.
				memMode = "";
					if (exitButton[0].getPosition().contains
						(mouseWorldPosition)) {
						// Since they clicked to exit, take them to the menu.
						state = State::INTRO;
					}
				
				} // End has drummed.
				
			}
			
			// We, of course, need to update the drums.
			for (int i = 0; i < numDrums; i++) {
				waterDrums[i].update(waterDrums[i].isActive(), dtAsSeconds, resolution.x);
			}
			
			
		} else if (state == State::FREESTYLE) {
			
			if (hasDrummed) {
				
				if (exitButton[0].getPosition().contains
						(mouseWorldPosition)) {
					// Since they clicked to exit, take them to the menu.
					state = State::INTRO;
				}
			
				for (int i = 0; i < numDrums; i++) {
				
					if (waterDrums[i].getPosition().contains
						(mouseWorldPosition)) {
						
						switch (i) {
							case 4:
								drumFive.play();
								break;

							case 3:
								drumFour.play();
								break;
								
							case 2:
								drumThree.play();
								break;
								
							case 1:
								drumTwo.play();
								break;

							default:
								drumOne.play();
								break;
							}
						
						hasDrummed = false;
						acceptInput = false;
						waterDrums[i].struck();
						scoreInt++;
						
					}
				
				}
				
			}
			
			// Update the score.
			std::stringstream ss;
			ss << "Score = " << scoreInt;
			scoreText.setString(ss.str());
			
			for (int i = 0; i < numDrums; i++) {
				waterDrums[i].update(waterDrums[i].isActive(), dtAsSeconds, resolution.x);
			}
			
			
			
		} else if (state == State::MEMORY) {
			
			if (greatJob) {
				greatJobText.setPosition(resolution.x /2.0f,resolution.y / 2);
				
				if (hasDrummed) {
					// Set the state back to Demo Mode for another sequence.
					state = State::DEMO;
					greatJobText.setPosition(-2000,-2000);
					greatJob = false;
				}
					
			} else {
			// Move the failText out of the way.
		    failText.setPosition(-2000,-2000);
			
			// Slowly decrease the time until none remains....
			timeTarget -= dtAsSeconds;
			timeBar.setSize(Vector2f(widthPerSecond * timeTarget, timeBarHeight));

			if (hasDrummed) {
				
				if (exitButton[0].getPosition().contains
						(mouseWorldPosition)) {
					// Since they clicked to exit, take them to the menu.
					state = State::INTRO;
				}
				
				String drum = "";
			
				for (int i = 0; i < numDrums; i++) {
				
					if (waterDrums[i].getPosition().contains
						(mouseWorldPosition)) {
						
						switch (i) {
							case 4:
								drumFive.play();
								drum = "4";
								break;

							case 3:
								drumFour.play();
								drum = "3";
								break;
								
							case 2:
								drumThree.play();
								drum = "2";
								break;
								
							case 1:
								drumTwo.play();
								drum = "1";
								break;

							default:
								drumOne.play();
								drum = "0";
								break;
							}
						
						hasDrummed = false;
						acceptInput = false;
						waterDrums[i].struck();
						scoreInt++;
						memMode = memMode + drum;
						
					}
				
				}
				
			}
			
			// Update the score.
			std::stringstream ss;
			ss << "Score = " << scoreInt;
			scoreText.setString(ss.str());
			
			for (int i = 0; i < numDrums; i++) {
				waterDrums[i].update(waterDrums[i].isActive(), dtAsSeconds, resolution.x);
			}
			
			if (memMode.length() >= demoMode.length()) {
				
				// This round is over, for better or worse.
				if (memMode == demoMode) {
					// Great Job!
					// Add 2 to the power of the length of the memory sequence!
					scoreInt += (2^memMode.length());
					
					// Update the score.
					std::stringstream ss;
					ss << "Score = " << scoreInt;
					scoreText.setString(ss.str());
					
					// Set the variables for the next round.
					roundNum++;
					demoShown = false;
					demoReady = false;
					demoSound = true;
					greatJob = true;
					
					
				} else {
					// Oops! Try again!
				    failText.setPosition(resolution.x /2.0f,resolution.y / 2);
					// Lower the number of tries left.
					tryInt--;
					// Update the score.
					std::stringstream ss;
					ss << "Tries left = " << tryInt;
					tryText.setString(ss.str());
					
					if (tryInt >= 0) {
						/*
						 * Set demoShown to false, so it will show the pattern
						 * again.
						 */
						demoShown = false;
						demoReady = false;
						demoSound = true;
						currentDemo = 1;
						timeTarget = 1 * roundNum;
						state = State::DEMO;
						// Move the failText out of the way.
						failText.setPosition(-2000,-2000);
					} else if (tryInt < 0) {
						// Game over!!!
						state = State::INTRO;
						// Move the failText out of the way.
						failText.setPosition(-2000,-2000);
					} // end check tries.
					
				} // end pass/fail.
				
			} // end length check.
			
			if (timeTarget <= 0) {
					// Oops! Try again!
				    failText.setPosition(resolution.x /2.0f,resolution.y / 2);
					// Lower the number of tries left.
					tryInt--;
					// Update the score.
					std::stringstream ss;
					ss << "Tries left = " << tryInt;
					tryText.setString(ss.str());
					
					if (tryInt >= 0) {
						/*
						 * Set demoShown to false, so it will show the pattern
						 * again.
						 */
						demoShown = false;
						demoReady = false;
						demoSound = true;
						currentDemo = 1;
						timeTarget = 1 * roundNum;
						state = State::DEMO;
						// Move the failText out of the way.
						failText.setPosition(-2000,-2000);
					} else if (tryInt < 0) {
						// Game over!!!
						state = State::INTRO;
						// Move the failText out of the way.
						failText.setPosition(-2000,-2000);
					} // end check tries.
			} // end time fail.
		} // end of not great job.
		} // end memory state.
			
		
		/* 
		 * 
		 *  Draw the scene.
		 * 
		 */		
		
			window.clear();
			window.draw(spriteBackground);
						
		if (state == State::INTRO) {
			
			window.setView(mainView);
			window.setView(hudView);
			
		} else if (state == State::MENU) {
			
			window.setView(mainView);
				for (int i = 0; i < numMenuItems; i++) {
					window.draw(menuItems[i].getSprite());
				}
			window.setView(hudView);
				
				window.draw(menuText);
				window.draw(freeStyleText);
				window.draw(memoryText);
				window.draw(exitText);
						
		} else if (state == State::DEMO) {
			window.setView(mainView);
				for (int i = 0; i < numDrums; i++) {
					window.draw(waterDrums[i].getSprite());
				}
				window.draw(spriteRHand);
				window.draw(readyText);
			window.setView(hudView);
				window.draw(scoreText);
				window.draw(tryText);
				window.draw(exitButton[0].getSprite());
			
		} else if (state == State::FREESTYLE) {
			
			window.setView(mainView);
				for (int i = 0; i < numDrums; i++) {
					window.draw(waterDrums[i].getSprite());
				}
			window.setView(hudView);
				window.draw(scoreText);
				window.draw(exitButton[0].getSprite());
						
		} else if (state == State::MEMORY) {
			window.setView(mainView);
				for (int i = 0; i < numDrums; i++) {
						window.draw(waterDrums[i].getSprite());
					}

			window.setView(hudView);
				window.draw(timeBar);
				window.draw(failText);
				window.draw(scoreText);
				window.draw(tryText);
				window.draw(greatJobText);
				window.draw(exitButton[0].getSprite());
			
		}
		
			window.draw(spriteDrumHand);
			window.display();
		
    }
    // Clean up any mess left over....
	delete[] menuItems;
	delete[] exitButton;
	delete[] waterDrums;
    return 0;
}
