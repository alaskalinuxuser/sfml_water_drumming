#include <sstream>
#include <ExitButton.h>
#include <TextureHolder.h>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace sf;

void ExitButton::spawn(float sResolutionX, float sResolutionY, int itemNum) {
	
	m_Sprite = Sprite(TextureHolder::GetTexture(
		"graphics/exit.png"));
		
	m_Sprite.setPosition(sResolutionX - 100,sResolutionY - 100);
	m_Sprite.setScale((sResolutionX *.04)/100,(sResolutionX *.04)/100);
	m_Number = itemNum;
}
FloatRect ExitButton::getPosition() {
	return m_Sprite.getGlobalBounds();
}

Vector2f ExitButton::getCoordPosition() {
	return m_Sprite.getPosition();
}

Sprite ExitButton::getSprite() {
	return m_Sprite;
}
