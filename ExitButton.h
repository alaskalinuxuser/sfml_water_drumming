#pragma once
#include <SFML/Graphics.hpp>
#include <cmath>

using namespace sf;

class ExitButton {
	
private:
	
	// Where is this MenuItems?
	Vector2f m_Position;

	// A sprite for the MenuItems
	Sprite m_Sprite;
	
	int m_Number;

	// Public prototypes go here	
public:

	// Spawn a new MenuItems
	void spawn(float sResolutionX, float sResolutionY, int itemNum);

	// Return a rectangle that is the position in the world
	FloatRect getPosition();
	
	// Return coordinate position
	Vector2f getCoordPosition();

	// Get a copy of the sprite to draw
	Sprite getSprite();
};


